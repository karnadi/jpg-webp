const imagemin = require('imagemin');
const imageminWebp = require('imagemin-webp');
const path = "images/"
imagemin([path+'*.{jpg,png}'], path+'webp/', {
	use: [
		imageminWebp({quality: 50, method:6})
	]
}).then(() => {
	console.log('Images optimized');
});